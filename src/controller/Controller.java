package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String dato2 = "";
		String respuesta = "";
		double num1=0;
		double num2=0;
		int num3=0;
		int num4=0;
		int num5=0;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				//int capacidad = lector.nextInt();
				modelo = new MVCModelo(); 
				modelo.cargarViajes();
				modelo.cargarNodos();
				modelo.cargarZona();
				System.out.println("Archivo CSV cargado");
				view.printCargar(modelo.darNumeroViajes(), modelo.darNumeroZonas(), modelo.darNumeroNodos());
//				view.printCargarViajes(modelo.darTotalViajesMes(), modelo.darTotalViajesDia(), modelo.darTotalViajesHora(), modelo.darZonaMenor(), modelo.darZonaMayor());					
				
				break;

			case 2:
				//REQ 1A
				System.out.println("--------- \nDar N: ");
				int N = lector.nextInt();
				view.printLetrasMasFrecuentes(modelo.consultarLetrasMasFrecuentes(N), N);
				break;

			case 3:
				//REQ2A	
				System.out.println("--------- \nDar latitud (Utilice , como separador decimal): ");
				double lat = lector.nextDouble();
				System.out.println("--------- \nDar longitud (Utilice , como separador decimal): ");
				double lon = lector.nextDouble();
				view.printNodosDelimitan(modelo.consultarNodosDelimitan(lat, lon), modelo.consultarNodosNombres(lat, lon));
				break;
			case 4:
				//REQ3A
				System.out.println("--------- \nDar limite bajo: ");
				num1=lector.nextDouble();
				System.out.println("--------- \nDar limite alto: ");
				num2=lector.nextDouble();
				view.printViajesMesTiempo(modelo.consultarViajesPorTiempo(num1, num2));
				break;
			case 5:
				//REQ1B
				System.out.println("--------- \nDar N: ");
				view.printZonasNorte(modelo.consultarZonasMasAlNorte(lector.nextInt()));
				break;

			case 6:
				//REQ2B
				System.out.println("--------- \nDar Latitud: ");
				num1=lector.nextDouble();
				System.out.println("--------- \nDar Longitud: ");
				num2=lector.nextDouble();
				view.printNodosMalla(modelo.consultarNodosEnMallaVial(num1, num2));
				break;	
			case 7:
				//REQ3B
				System.out.println("--------- \nDar limite bajo: ");
				num1=lector.nextDouble();
				System.out.println("--------- \nDar limite alto: ");
				num2=lector.nextDouble();
				view.printViajesMesDesviacion(modelo.consultarViajesPorDesviacion(num1, num2));
				break;

			case 8:
				//REQ1C
				System.out.println("--------- \nDar Sourceid: ");
				num3=lector.nextInt();
				System.out.println("--------- \nDar Hora: ");
				num4=lector.nextInt();
				view.printViajesHoraSource(modelo.consultarViajesSource(num3, num4));
				break;	
			case 9:
				//REQ2C
				System.out.println("--------- \nDar Destid: ");
				num3=lector.nextInt();
				System.out.println("--------- \nDar Hora minima: ");
				num4=lector.nextInt();
				System.out.println("--------- \nDar Hora maxima: ");
				num5=lector.nextInt();
				view.printViajesHoraSource(modelo.consultarViajesDest(num3, num4,num5));
				break;

			case 10:
				//REQ3C
				System.out.println("--------- \nDar N: ");
				num3=lector.nextInt();
				view.printZonasPriorizadas(modelo.consultarZonasPriorizadas(num3));
				break;	
			case 11:
				view.printMessage(modelo.generarGrafica());
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
