package model.data_structures;


/**
 * @author leone
 * @version 1.0
 */
public interface IQueue<T> {

	/**
	 * 
	 * @param pItem
	 */
	public void enqueue(T pItem);

	public T dequeue();

	public int getSize();

	public boolean isEmpty();

	public T getFirst();

}