package model.data_structures;

import java.util.Iterator;

public class Node<Key,Value>
{
	private Key key;
	private Queue<Value> valueSet;
	private Node<Key,Value> siguiente;

	public Node(Key pkey, Value pvalue, Node<Key,Value> pNodo)
	{
		key=pkey;
		valueSet = new Queue<Value>();
		valueSet.enqueue(pvalue);
		this.siguiente=pNodo;
	}
	public Node<Key,Value> darSiguiente()
	{
		return siguiente;
	}
	public Value getValue() 
	{
		return valueSet.getFirst();
	}
	public Iterator<Value> getValues()
	{
		return valueSet;
	}
	public Key getKey() 
	{
		return key;
	}
	public void cambiarSiguiente(Node<Key,Value> nodo)
	{
		siguiente=nodo;
	}
	public void cambiarValor(Value pValue)
	{
		valueSet = new Queue<Value>();
		valueSet.enqueue(pValue);
	}
	public void agregarValor(Value pValue)
	{
		valueSet.enqueue(pValue);
	}
}
