package model.data_structures;

import java.util.Iterator;

/**
 * @author leone
 * @version 1.0
 */
public class Queue<T> implements IQueue<T>, Iterator<T>{

	private Node<T> first;
	private Node<T> last;
	private int size;

	public Queue()
	{
		size = 0;
		first = null;
		last = null;
	}

	/**
	 * 
	 * @param pItem
	 */
	public void enqueue(T pItem)
	{
		Node<T> old = last;
		last = new Node<T>(pItem);
		if (isEmpty()) 
			first = last;
		else
			old.cambiarSiguiente(last);
		size++;
	}

	public T dequeue()
	{
		T eliminado = null;
		if (first != null) 
		{
			eliminado = first.darElemento();
			first = first.darSiguiente();
			size--;
		} 
		if(isEmpty())
			last = null;
		return eliminado;
	}

	public int getSize(){
		return size;

	}

	public boolean isEmpty()
	{
		return size==0;
	}

	public T getFirst(){
		return first.darElemento();
	}
	
	private class Node<T>
	{
		private T elemento;
		private Node<T> siguiente;

		public Node(T pElemento)
		{
			elemento = pElemento;
			this.siguiente=null;
		}
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		public T darElemento() 
		{
			return elemento;
		}
		public void cambiarSiguiente(Node<T> nodo)
		{
			siguiente=nodo;
		}
	}

	@Override
	public boolean hasNext() {
		return !isEmpty();
	}

	@Override
	public T next() {
		return dequeue();
	}

}//end Queue