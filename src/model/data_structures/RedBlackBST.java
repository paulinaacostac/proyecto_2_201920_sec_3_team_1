package model.data_structures;
import java.util.Iterator;

/**
 * @author Team 1
 * @version 1.0
 */
public class RedBlackBST<Key extends Comparable<Key>, Value> {

	public Node root;
	
	public final static boolean RED = true;
	public final static boolean BLACK = false;
	
	
	private class Node{
		Key key;
		Value value;
		Node left;
		Node right;
		int N;
		boolean color;
		
		public Node(Key key, Value value, int N, boolean color )
		{
			this.key=key;
			this.value=value;
			this.N=N;
			this.color=color;
		}
	}

	public RedBlackBST()
	{
		root=null;
	}

	public int size()
	{
		return size(root);
	}
	
	public int size(Node h)
	{
		if(h==null) return 0;
		return h.N;
	}

	public boolean isEmpty(){
		return root==null;
	}
	
	public int height()
	{
		return height(root);
	}
	
	/**
	 * 
	 * @param key
	 */
	public int getHeight(Key key)
	{
		int height = -1;
		boolean encontrado=false;
		Node x = root;
		int cmp;
		while (x != null && !encontrado) 
		{
			cmp = key.compareTo(x.key);
			if(cmp<0) x=x.left;
			else if (cmp>0) x=x.right;
			else encontrado = true;
			height++;
		}
		if (!encontrado)
			height = -1;
		return height;
	}

	/**
	 * 
	 * @param key
	 */
	public boolean constains(Key key)
	{
		return get(key)!=null;
	}

	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void put(Key key, Value value)
	{
		root =put(root,key,value);
		root.color=BLACK;
	}

	/**
	 * 
	 * @param h
	 * @param key
	 * @param value
	 */
	private Node put(Node h, Key key, Value value)
	{
		if (h==null) return new Node(key,value,1,RED);
		int cmp = key.compareTo(h.key);
		if (cmp<0) h.left = put(h.left, key, value);
		else if (cmp>0) h.right=put(h.right, key, value); 
		else h.value=value;
		
		if(isRed(h.right) && !isRed(h.left)) h = rotateLeft(h);
		if(isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if(isRed(h.right) && isRed(h.left)) flipColors(h);
	
		h.N = size(h.left) + size(h.right)+1;
		
		return h;
	}

	/**
	 * 
	 * @param key
	 */
	public Value get(Key key)
	{
		return get(root, key);
	}

	/**
	 * 
	 * @param h
	 * @param key
	 */
	private Value get(Node h, Key key)
	{
		Node x = h;
		int cmp;
		while (x != null) 
		{
			cmp = key.compareTo(x.key);
			if(cmp<0) x=x.left;
			else if (cmp>0) x=x.right;
			else return x.value;
		}
		return null;
	}

	public Key min(){
		return  min(root).key;
	}

	public Key max(){
		return max(root).key;
	}

	public Iterator<Key> keys()
	{
		Queue<Key> queue = new Queue<Key>();
		keys(root, queue, min(), max());
		return queue;
	}

	/**
	 * 
	 * @param init
	 * @param end
	 */
	public Iterator<Key> keysInRange(Key init, Key end)
	{
		Queue<Key> queue = new Queue<Key>();
		keys(root, queue, init, end);
		return queue;
	}

	/**
	 * 
	 * @param init
	 * @param end
	 */
	public Iterator<Value> valuesInRange(Key init, Key end)
	{
		Queue<Value> queue = new Queue<Value>();
		values(root, queue, init, end);
		return queue;
	}

	/**
	 * 
	 * @param x
	 * @param queue
	 * @param init
	 * @param end
	 */
	private void keys(Node h, Queue<Key> queue, Key init, Key end)
	{
		if (h == null) return;
		int cmpMin = init.compareTo(h.key);
		int cmpMax = end.compareTo(h.key);
		if (cmpMin<0) keys(h.left, queue, init, end);
		if (cmpMin <= 0 && cmpMax >= 0) queue.enqueue(h.key);
		if (cmpMax > 0) keys(h.right, queue, init, end);
	}
	
	/**
	 * 
	 * @param x
	 * @param queue
	 * @param init
	 * @param end
	 */
	private void values(Node h, Queue<Value> queue, Key init, Key end)
	{
		if (h == null) return;
		int cmpMin = init.compareTo(h.key);
		int cmpMax = end.compareTo(h.key);
		if (cmpMin<0) values(h.left, queue, init, end);
		if (cmpMin < 0 && cmpMax > 0) queue.enqueue(h.value);
		if (cmpMax > 0) values(h.right, queue, init, end);
	}

	/**
	 * 
	 * @param h
	 */
	private Node min(Node h){
		Node x = h;
		while(x.left !=null)
		{
			x = x.left;
		}
		return x;
	}

	/**
	 * 
	 * @param h
	 */
	private Node max(Node h){
		Node x = h;
		while(x.right !=null)
		{
			x = x.right;
		}
		return x;
	}

	/**
	 * 
	 * @param h
	 */
	private int height(Node h)
	{
		if (h == null) return -1;
		return Math.max(height(h.left), height(h.right))+1;
	}
	public int heightBlack()
	{
		return heightBlack(root);
	}
	public int heightBlack(Node h)
	{
		if (h == null) return -1;
		if (isRed(h)) return Math.max(height(h.left), height(h.right));
		else return Math.max(height(h.left), height(h.right))+1;
	}

	/**
	 * 
	 * @param h
	 */
	private Node rotateLeft(Node h)
	{
        Node x = h.right;
        h.right = x.left;
        x.left = h;
        x.color = x.left.color;
        x.left.color = RED;
        x.N = h.N;
        h.N = size(h.left) + size(h.right) + 1;
		return x;
	}

	/**
	 * 
	 * @param h
	 */
	private Node rotateRight(Node h)
	{
        Node x = h.left;
        h.left = x.right;
        x.right = h;
        x.color = x.right.color;
        x.right.color = RED;
        x.N = h.N;
        h.N = size(h.left) + size(h.right) + 1;
		return x;
	}

	/**
	 * 
	 * @param h
	 */
	private void flipColors(Node h)
	{
		
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}
	
	private boolean isRed(Node h)
	{
		if (h == null) return false;
		else return h.color==RED;
	}
	
	public boolean check()
	{
		return isBST() && is23() && isBalanced();
	}
	
	private boolean isBST()
	{
		return isBST(root);
	}
	
	private boolean isBST(Node h)
	{
		if (h == null) return true;
		if (h.left != null && h.left.key.compareTo(h.key)>0 ) return false;
		if (h.right != null && h.right.key.compareTo(h.key)<0 ) return false;
		return isBST(h.left) && isBST(h.right);
	}
	private boolean is23()
	{
		return is23(root);
	}
	
	private boolean is23(Node h)
	{
		//Caso base
		if (h == null) return true;
		//Nodo derecho rojo
		if (isRed(h.right)) return false;
		//Doble rojo seguido 
		if (h != root && isRed(h) && isRed(h.left)) return false;
		return is23(h.left) && is23(h.right);
	}
	
	private boolean isBalanced()
	{
		int contador=0;
		Node x = root;
		//Contar todos los enlaces negros
		while (x != null) 
		{
			if(!isRed(x)) contador++;
			x = x.left;
		}
		return isBalanced(root, contador);
	}
	
	private boolean isBalanced(Node h, int contador)
	{
		if (h ==null ) return contador == 0;
		if(!isRed(h)) contador--; 
		return isBalanced(h.right, contador) && isBalanced(h.left, contador);
	}
	
	public Iterator<Key> getLeafs()
	{
		Queue<Key> leafs = new Queue<Key>();
		getLeafs(root,leafs);
		return leafs;
	}
	
	private void getLeafs(Node x, Queue<Key> leafs)
	{
		if (x==null) return;
		getLeafs(x.left, leafs);
		if (x.left==null && x.right == null) leafs.enqueue(x.key);
		getLeafs(x.right, leafs);
	}
	
}//end RedBlackBST