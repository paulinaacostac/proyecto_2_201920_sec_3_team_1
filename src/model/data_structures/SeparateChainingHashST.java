package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashST<Key extends Comparable<Key>, Value>
{
	private int M;
	private int N;
	private SequentialSearchST<Key, Value>[] st; 
	private int numRehash;
	
	public SeparateChainingHashST()
	{ 
		this.M = 5001;
		N = 0;
		st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[M];
		for (int i = 0; i < M; i++)
			st[i] = new SequentialSearchST<Key,Value>();
	}
	
	public SeparateChainingHashST(int M)
	{ 
		this.M = M;
		N = 0;
		st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[M];
		for (int i = 0; i < M; i++)
			st[i] = new SequentialSearchST<Key,Value>();
	}
	private int hash(Key key)
	{ 
		return (key.hashCode() & 0x7fffffff) % M; 
	}
	public Value get(Key key)
	{ 
		return (Value) st[hash(key)].get(key); 
	}
	
	public Iterator<Value> getSet(Key key)
	{
		return (Iterator<Value>) st[hash(key)].getSet(key);
	}
	public void put(Key key, Value val)
	{
		if (!contains(key)) {
			N++;
		}
		st[hash(key)].put(key, val);
		if (N/M > 5) resize(nextPrime(M));
	}
	
	public void putInSet(Key key, Value val)
	{
		if (!contains(key)) {
			N++;
		}
		st[hash(key)].putInSet(key, val);
		if (N/M > 5) resize(2*M);
	}
	
	public Value delete(Key key)
	{
		Value deleted = st[hash(key)].delete(key);
		if (deleted != null) 
		{
			N--;
		}
		return deleted;
	}
	
	public Iterator<Value> deleteSet(Key key)
	{
		Iterator<Value> deleted = st[hash(key)].deleteSet(key);
		if (deleted != null) {
			N--;
		}
		return deleted;
	}
	
	public Iterator<Key> keys()
	{
		Queue<Key> keys = new Queue<Key>();
		for (int i = 0; i < st.length; i++) {
			if (st[i].getsize() > 0) 
			{
				Iterator<Key> keysSub = st[i].getKeys();
				while (keysSub.hasNext()) {
					Key key = (Key) keysSub.next();
					keys.enqueue(key);
				}
			}
		}
		return keys;
	}
	
	public void resize(int cap)
	{
		SeparateChainingHashST<Key, Value> t;
		t = new SeparateChainingHashST<Key, Value>(cap);
		for (int i = 0; i < st.length; i++)
		{
			if (st[i].getsize() > 0) 
			{
				Iterator<Key> keysSub = st[i].getKeys();
				while (keysSub.hasNext()) 
				{
					Key key = (Key) keysSub.next();
					Iterator<Value> valuesSub = getSet(key);
					while (valuesSub.hasNext()) 
					{
						Value value = (Value) valuesSub.next();
						t.putInSet(key, value);
					}
				}
			}
		}
		N = t.N;
		M = t.M;
		st = t.st;
		numRehash++;
	}
	public int darNumRehash()
	{
		return numRehash;
	}

	public int nextPrime(int n)
	{
		int nextPrime = n;
		if (n%2 == 0)
			nextPrime = n+1;
		else
			nextPrime = n+2;
		
		boolean found = false;
		while (!found)
		{
			boolean isPrime = true;
			for (int i = 2; i < nextPrime && isPrime; i++) 
			{
				if (nextPrime%i == 0) 
				{
					isPrime=false;
				}
			}
			if (isPrime) 
			{
				found=true;
			}
			else
			{
				nextPrime+=2;
			}
		}
		
		return nextPrime;
	}
	

	public int darN() {
		return N;
	}
	
	public int darM() {
		return M;
	}
	
	public boolean contains(Key key)
	{
		return get(key)!=null;
	}
	
}