package model.logic;

public class Coordenada implements Comparable<Coordenada>
{
	private double longitud;
	private double latitud;
	public Coordenada(double pLongitud,double pLatitud)
	{
		longitud=pLongitud;
		latitud=pLatitud;
	}
	public double darLongitud()
	{
		return longitud;
	}
	public double darLatitud()
	{
		return latitud;
	}
	public int compareTo(Coordenada c)
	{
		if (this.latitud<c.latitud)
		{
			return -1;
		}
		if (this.latitud>c.latitud)
		{
			return 1;
		}
		if (this.latitud==c.latitud)
		{
			return 0;
		}
		return 0;
	}
	
}
