package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.google.gson.Gson;
import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxHeapCP;
import model.data_structures.MinHeapCP;
import model.data_structures.RedBlackBST;
/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private int totalViajesHora;
	private int totalViajesDia;
	private int totalViajesMes;
	private int numeroViajes;
	private int numeroZonas;
	private int numeroNodos;	
	private RedBlackBST<Double,ViajeMes> listaViajesMes;
	private ArregloDinamico<ViajeMes> arregloViajesMes;
	private ArregloDinamico<ViajeDia> listaViajesDia;
	private ArregloDinamico<ViajeHora> listaViajesHora;
	private ArregloDinamico<Esquina> esquinas;
	private ArregloDinamico<Zona> zonas;
	private ArregloDinamico<ZonaFrontera> zonasFrontera;
	private LinearProbingHashST<String, Zona> zonasST;

	public MVCModelo()
	{
		zonasST = new LinearProbingHashST<String, Zona>();
		listaViajesMes=new RedBlackBST<Double,ViajeMes>();
		arregloViajesMes=new ArregloDinamico<ViajeMes>(50000);
		listaViajesDia=new ArregloDinamico<ViajeDia>(50000);
		listaViajesHora=new ArregloDinamico<ViajeHora>(50000);
		esquinas=new ArregloDinamico<Esquina>();
		zonas = new ArregloDinamico<Zona>(500);
		zonasFrontera=new ArregloDinamico<ZonaFrontera>(1000);
		totalViajesDia=0;
		totalViajesHora=0;
		totalViajesMes=0;
		numeroNodos=0;
		numeroViajes=0;
		numeroZonas=0;
	}

	public void cargarZona()
	{
		Gson gson = new Gson();
		try (Reader reader = new FileReader("./data/bogota_cadastral.json")) 
		{
			ArregloJSON az=gson.fromJson(reader, ArregloJSON.class);
			//			System.out.println(az.darFeatures()[0].getProperties().toString());
			numeroZonas=az.darFeatures().length;
			for (int i=0;i<az.darFeatures().length;i++)
			{
				String sourceId=az.darFeatures()[i].getProperties().getMOVEMENTID();
				String nombre=az.darFeatures()[i].getProperties().getScanombre();
				double shape_leng=az.darFeatures()[i].getProperties().getShapeLeng();
				double shape_area=az.darFeatures()[i].getProperties().getShapeArea();
				double longitud=0.0;
				double latitud=0.0;
				double[][][][] coordenadas=az.darFeatures()[i].getGeometry().getCoordinates();
				int numCoordenadas=coordenadas[0][0].length;
				MaxHeapCP<Coordenada> coordenadasDMax=new MaxHeapCP<Coordenada>(700);
				for (int j=0;j<numCoordenadas;j++)
				{
					longitud=coordenadas[0][0][j][0];
					latitud=coordenadas[0][0][j][1];
					Coordenada coord=new Coordenada(longitud,latitud);
					coordenadasDMax.insertar(coord);
					//						System.out.println(j);
					//						System.out.println("longitud: "+longitud+" latitud: "+latitud);
				}
				Zona zona=new Zona(sourceId, nombre, shape_leng, shape_area, coordenadasDMax);
				ZonaFrontera z=new ZonaFrontera(sourceId, nombre, shape_leng, shape_area, coordenadasDMax);
				zonas.agregar(zona);
				zonasFrontera.agregar(z);
			}
//			for (int i=0;i<zonas.darElemento(0).darCoordenadas().size();i++)
//			{
//				Coordenada c=zonas.darElemento(0).darCoordenadas().sacarMax();
//				Coordenada d=zonas.darElemento(0).darCoordenadas().darMax();
//				System.out.println("Latitud: "+c.darLatitud());
//				System.out.println("Longitud: "+c.darLongitud());
//				System.out.println("------------------------------");
//			}
//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

	}
	public void cargarNodos()
	{
		File file = new File("./data/Nodes.txt");
		BufferedReader br = null;
		String nextLine="";
		try 
		{
			br = new BufferedReader(new FileReader(file));
			while ((nextLine = br.readLine()) != null)
			{
				String partes[]=nextLine.split(",");
				Esquina esquina = new Esquina(Integer.parseInt(partes[0]), Double.parseDouble(partes[1]), Double.parseDouble(partes[2])); 
				esquinas.agregar(esquina);
				numeroNodos++;
			}
		}
		catch (NumberFormatException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void cargarViajes()
	{
		CSVReader reader = null;
		CSVReader reader1 = null;
		CSVReader reader2 = null;
		int nHora=0;
		int nDia=0;
		int nMes=0;
		try 
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader1= new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv"));
			reader2= new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-WeeklyAggregate.csv"));
			reader.skip(1);
			reader1.skip(1);
			reader2.skip(1);
			{
				for(String[] nextLine : reader) 
				{
					ViajeHora viaje = new ViajeHora(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
					listaViajesHora.agregar(viaje);
					nHora++;
				}
				totalViajesHora=nHora;
				for(String[] nextLine : reader1) 
				{
					ViajeMes viaje = new ViajeMes(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
					listaViajesMes.put(Double.parseDouble(nextLine[4]),viaje);
					//System.out.println(nMes);
					arregloViajesMes.agregar(viaje);
					nMes++;
				}
				totalViajesMes=nMes;
//				System.out.println(nMes);
//				for(String[] nextLine : reader2) 
//				{
//					ViajeDia viaje = new ViajeDia(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
//					listaViajesDia.agregar(viaje);
//					nDia++;
//				}
//				totalViajesDia=nDia;
			}
			numeroViajes=totalViajesDia+totalViajesHora+totalViajesMes;
		}
		catch (FileNotFoundException e)  
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int darTotalViajesHora(){
		return totalViajesHora;
	}

	public int darTotalViajesDia(){
		return totalViajesDia;
	}  

	public int darTotalViajesMes(){
		return totalViajesMes;
	}
	public int darNumeroViajes()
	{
		return numeroViajes;
	}
	public int darNumeroZonas()
	{
		return numeroZonas;
	}
	public int darNumeroNodos()
	{
		return numeroNodos;
	}

	public ArregloDinamico<Zona> consultarLetrasMasFrecuentes(int N)
	{
		LinearProbingHashST<String, Zona> zonasPorLetra = new LinearProbingHashST<String, Zona>();
		RedBlackBST<String, Integer> contador = new RedBlackBST<String, Integer>();
		for (int i = 0; i < zonas.darTamano(); i++) 
		{
			int cuenta = 1;
			Zona zonaTemp = zonas.darElemento(i);
			String letraInicial = zonaTemp.darNombre().substring(0,1).toLowerCase();
			zonasPorLetra.putInSet(letraInicial, zonaTemp);
			
			if (contador.constains(letraInicial)) 
				cuenta = contador.get(letraInicial)+1;
			
			contador.put(letraInicial, cuenta);
		}
		ArregloDinamico<Zona> zonas= new ArregloDinamico<Zona>();
		for (int i = 0; i < N; i++) 
		{
			int max = -1;
			String letraMax = "";
			Iterator<String> letras = contador.keys();
			while (letras.hasNext()) 
			{
				String letra = (String) letras.next();
				int repeticiones = contador.get(letra);
				if (repeticiones>max) 
				{
					letraMax = letra;
					max = repeticiones;
				}
			}
			contador.put(letraMax, -1);
			Iterator<Zona> zonasLetra = zonasPorLetra.deleteSet(letraMax);
			while (zonasLetra.hasNext()) 
			{
				Zona zonaTemp = (Zona) zonasLetra.next();
				zonas.agregar(zonaTemp);
				
			}
		}		
		return zonas;
	}

	public ArregloDinamico<Coordenada> consultarNodosDelimitan(double pLatitud, double pLongitud)
	{
		zonasST = new LinearProbingHashST<String, Zona>();
		ArregloDinamico<Coordenada> coordenadasIgual = new ArregloDinamico<Coordenada>();
		
		DecimalFormat df=new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.DOWN);
		String latitud  = df.format(pLatitud);
		String longitud = df.format(pLongitud);
		System.out.println("Coordenadas:" +latitud + "-" + longitud);
		for (int i = 0; i < zonas.darTamano() ; i++) 
		{
			//System.out.println(i);
			Zona zonaTemp = zonas.darElemento(i);
			MaxHeapCP<Coordenada> coordenadas = zonaTemp.darCoordenadasMaxHeap();
			ArregloDinamico<Coordenada> coordenadasTodas = new ArregloDinamico<Coordenada>();
			while (!coordenadas.esVacia()) 
			{
				Coordenada coord = coordenadas.sacarMax();
				String latitudTemp  = df.format(coord.darLatitud()) ;
				String longitudTemp = df.format(coord.darLongitud());
				String key = df.format(coord.darLatitud()) +  "-" + df.format(coord.darLongitud());
				zonasST.putInSet(key, zonaTemp);
				if (latitud.equals(latitudTemp) && longitud.equals(longitudTemp)) 
				{
					coordenadasIgual.agregar(new Coordenada(coord.darLongitud(),coord.darLatitud()));
				}
				coordenadasTodas.agregar(coord);
			}
			for (int j = 0; j < coordenadasTodas.darTamano(); j++) 
			{
				coordenadas.insertar(coordenadasTodas.darElemento(j));
			}
		}
		return coordenadasIgual;
	}

	public ArregloDinamico<String> consultarNodosNombres(double pLatitud, double pLongitud)
	{
		ArregloDinamico<String> nombreZonas = new ArregloDinamico<String>();
		DecimalFormat df=new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.DOWN);
		String latitud  = df.format(pLatitud);
		String longitud = df.format(pLongitud);
		String key = latitud +  "-" + longitud;
		Iterator<Zona> zonasIgual= zonasST.getSet(key);
		if (zonasIgual != null) {
			while (zonasIgual.hasNext()) {
				Zona zona = (Zona) zonasIgual.next();
				nombreZonas.agregar(zona.darNombre());
			}
		}
		return nombreZonas;
	}

	public ArregloDinamico<ViajeMes> consultarViajesPorTiempo(double min,double max)
	{
		ArregloDinamico<ViajeMes> viajes = new ArregloDinamico<ViajeMes>(500);
		MinHeapCP<ViajeMes> viajesHeap = new MinHeapCP<ViajeMes>(500);
		for (int i = 0; i < arregloViajesMes.darTamano(); i++) 
		{
			ViajeMes temp = arregloViajesMes.darElemento(i);
			if (temp.darMeanTravelTime()>= min && temp.darMeanTravelTime()<= max) 
			{
				viajesHeap.insertar(temp);
			}
		}
		while (!viajesHeap.esVacia()) 
			viajes.agregar(viajesHeap.sacarMax());
		
		return viajes;
	}

	public ArregloDinamico<Zona> consultarZonasMasAlNorte(int N)
	{
		ArregloDinamico<Zona> zonass=new ArregloDinamico<Zona>(N);
		MaxHeapCP<Zona> colaZonas=new MaxHeapCP<Zona>();
		for (int i=0;i<zonas.darTamano();i++)
		{
			colaZonas.insertar(zonas.darElemento(i));
		}
		for (int i=0;i<N;i++)
		{	
			System.out.println(colaZonas.sacarMax().darLatitudMaxima());
			zonass.agregar(colaZonas.sacarMax());
		}
		return zonass;
	}

	public LinearProbingHashST<Integer,Esquina> consultarNodosEnMallaVial(double pLatitud, double pLongitud)
	{
		LinearProbingHashST<Integer,Esquina> hash=new LinearProbingHashST<Integer,Esquina>();
		DecimalFormat df=new DecimalFormat("#.##");
		double latitud=Double.valueOf(df.format(pLatitud));
		double longitud=Double.valueOf(df.format(pLongitud));
		for (int i=0;i<esquinas.darTamano();i++)
		{
			Esquina e=esquinas.darElemento(i);
			if (Double.valueOf(df.format(e.darLatitud()))==latitud&&Double.valueOf(df.format(e.darLongitud()))==longitud)
			{
				hash.put(e.darpIdNodo(), e);
			}
		}
		return hash;
	}

	public ArregloDinamico<ViajeMes> consultarViajesPorDesviacion(double min,double max)
	{
		ArregloDinamico<ViajeMes>v=new ArregloDinamico<>(100);
		Iterator<ViajeMes>iter=listaViajesMes.valuesInRange(min, max);
		while (iter.hasNext())
		{
			ViajeMes a=iter.next();
			System.out.println(a.darStandardDeviationTravelTime());
			v.agregar(a);
		}
		return v;
	}

	public ArregloDinamico<ViajeHora> consultarViajesSource(int pSourceId, int pHora)
	{
		ArregloDinamico<ViajeHora> l=new ArregloDinamico<ViajeHora>(10000);
		for (int i=0;i<listaViajesHora.darTamano();i++)
		{
			ViajeHora vh=listaViajesHora.darElemento(i);
			if (vh.darSourceid()==pSourceId && vh.darHod()==pHora)
			{
				l.agregar(vh);
			}
		}
		return l;
	}

	public ArregloDinamico<ViajeHora> consultarViajesDest(int pDestId, int pHoraMin, int pHoraMax)
	{
		ArregloDinamico<ViajeHora> ad=new ArregloDinamico<ViajeHora>();
		MaxHeapCP<ViajeHora> cp=new MaxHeapCP<ViajeHora>();
		ViajeHora act=new ViajeHora(0, 0, 0, 0, 0, 0, 0);
		for (int i=0;i<listaViajesHora.darTamano();i++)
		{
			ViajeHora vh=listaViajesHora.darElemento(i);
			if (vh.darDstid()==pDestId)
			{
				cp.insertar(vh);
			}
		}
		while(!cp.esVacia())
		{
			if (cp.darMax().darHod()==pHoraMax)
			{
				//System.out.println("Entro");
				while (cp.darMax().darHod()!=pHoraMin)
				{
					act=cp.sacarMax();
					ad.agregar(act);
				}
				ad.agregar(cp.sacarMax()); //Agrega el viaje hora de pHoraMin
			}
			cp.sacarMax();
		}
		return ad;
		
		
//		RedBlackBST<Integer, ViajeHora> rb=new RedBlackBST<Integer,ViajeHora>();
//		for (int i=0; i<listaViajesHora.darTamano();i++)
//		{
//			ViajeHora vh=listaViajesHora.darElemento(i);
//			rb.put(""+vh.darDstid()+"-"+vh.darDow(), vh);
//		}
//		rb.keysInRange(""+pDestId+pHoraMin, ""+pDestId+pHoraMax);
	}

	public ArregloDinamico<ZonaFrontera> consultarZonasPriorizadas(int N)
	{
		ArregloDinamico<ZonaFrontera> c=new ArregloDinamico<ZonaFrontera>(1000);
		MaxHeapCP<ZonaFrontera> cp=new MaxHeapCP<ZonaFrontera>();
		for (int i=0;i<zonasFrontera.darTamano();i++)
		{
			cp.insertar(zonasFrontera.darElemento(i));
		}
		for (int i=0;i<N&&!cp.esVacia();i++)
		{
			c.agregar(cp.sacarMax());
		}
		return c;
	}

	public ArregloDinamico<String> consultarZonasPriorizadasNombre(ArregloDinamico<Zona> pZonas)
	{
		return null;
	}

	public ArregloDinamico<Esquina> consultarZonasPriorizadasNodo(ArregloDinamico<Zona> pZonas)
	{
		return null;
	} 

	public String generarGrafica()
	{
		LinearProbingHashST<Integer, Integer> contadorPorSourceId = new LinearProbingHashST<Integer, Integer>();
		String gr�fica = "--------------------------------------------------- \n";
		gr�fica+= "Porcentaje de datos faltantes por zona: \n";
		int idMax=0;
		int idMin=Integer.MAX_VALUE;;
		for (int i = 0; i < listaViajesHora.darTamano(); i++) {
			ViajeHora viajeTemp = listaViajesHora.darElemento(i);
			int cuenta = 1;
			
			if(contadorPorSourceId.get(viajeTemp.darSourceid())!=null)
				cuenta = contadorPorSourceId.get(viajeTemp.darSourceid())+1;
			
			contadorPorSourceId.put(viajeTemp.darSourceid(), cuenta);
			
			if (viajeTemp.darSourceid()>idMax)
				idMax = viajeTemp.darSourceid();
			if (viajeTemp.darDstid()>idMax)
				idMax = viajeTemp.darSourceid();
		
			if (viajeTemp.darSourceid()<idMin)
				idMin = viajeTemp.darSourceid();
			if (viajeTemp.darDstid()<idMin)
				idMin = viajeTemp.darSourceid();
		}
		int totalIds = (idMax-idMin+1);
		int viajesPorZona = totalIds*24;
		int i = idMin;
		while (i<=idMax) 
		{
			int hay = 0;
			if (contadorPorSourceId.get(i)!=null) 
				hay = contadorPorSourceId.get(i);
			double porcentajeHay = (double)hay/(double)viajesPorZona;
			double porcentajeFaltantes = 1-porcentajeHay;
			int estrellas = (int) (porcentajeFaltantes/0.02);
			String mensaje = i + "|";
			for (int j = 0; j < estrellas; j++) 
			{
				mensaje+="*";
			}
			mensaje+="\n";
			gr�fica+=mensaje;
			i++;
		}
		
		return gr�fica;
	}







}
