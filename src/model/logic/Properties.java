package model.logic;

import com.google.gson.annotations.SerializedName;

public class Properties 
{
	private int cartodb_id;
	
	private String scacodigo;
	
	private double shape_leng;
	
	private double shape_area;
	
	private String MOVEMENT_ID;
	
	private String DISPLAY_NAME;
	
	private int scatipo;
	
	private String scanombre;
	
	public int getCartodbId() 
	{
		return cartodb_id;
	}

	public void setCartodbId(int cartodbId) 
	{
		cartodb_id = cartodbId;
	}

	public String getScacodigo() 
	{
		return scacodigo;
	}

	public void setScacodigo(String pscacodigo) 
	{
		scacodigo = pscacodigo;
	}

	public int getScatipo() 
	{
		return scatipo;
	}

	public void setScatipo(int pscatipo) 
	{
		scatipo = pscatipo;
	}

	public String getScanombre() 
	{
		return scanombre;
	}

	public void setScanombre(String pscanombre) 
	{
		scanombre = pscanombre;
	}

	public double getShapeLeng() 
	{
		return shape_leng;
	}

	public void setShapeLeng(double shapeLeng) {
		shape_leng = shapeLeng;
	}

	public double getShapeArea() 
	{
		return shape_area;
	}

	public void setShapeArea(double shapeArea) {
		shape_area = shapeArea;
	}

	public String getMOVEMENTID() 
	{
		return MOVEMENT_ID;
	}

	public void setMOVEMENTID(String mOVEMENTID) 
	{
		MOVEMENT_ID = mOVEMENTID;
	}

	public String getDISPLAYNAME() 
	{
		return DISPLAY_NAME;
	}

	public void setDISPLAYNAME(String dISPLAYNAME) 
	{
		DISPLAY_NAME = dISPLAYNAME;
	}
	public String toString()
	{
		return "cartodb_id: "+cartodb_id+" scacodigo: "+scacodigo+" shape_leng: "+shape_leng+" shape_area: "+shape_area+" MOVEMENT_ID: "+ MOVEMENT_ID+" DISPLAY_NAME: "+" scatipo: "+scatipo+" scanombre: "+scanombre;
	}

}
