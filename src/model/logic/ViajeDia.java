package model.logic;

public class ViajeDia extends Viaje implements Comparable<ViajeDia> {

	private int dow;

	/**
	 * 
	 * @param psourceid
	 * @param pdstid
	 * @param pdow
	 * @param pmeantraveltime
	 * @param pstandarddeviationtraveltime
	 * @param pgeometricmeantraveltime
	 * @param pgeometricstandarddeviationtraveltime
	 */
	public ViajeDia(int psourceid, int pdstid, int pdow, double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime)
	{
		super(psourceid,pdstid,pmeantraveltime,pstandarddeviationtraveltime,pgeometricmeantraveltime,pgeometricstandarddeviationtraveltime);
		dow = pdow;
	}

	public  int darDow(){
		return dow;
	}
	@Override
	public int compareTo(ViajeDia o) {
		// TODO Auto-generated method stub
		return 0;
	}
}//end ViajeDia