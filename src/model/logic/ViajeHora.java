package model.logic;

public class ViajeHora extends Viaje implements Comparable<ViajeHora> 
{

	private int hod;
	
	/**
	 * 
	 * @param psourceid
	 * @param pdstid
	 * @param pHod
	 * @param pmeantraveltime
	 * @param pstandarddeviationtraveltime
	 * @param pgeometricmeantraveltime
	 * @param pgeometricstandarddeviationtraveltime
	 */
	public ViajeHora(int psourceid, int pdstid, int pHod, double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime){
		super(psourceid,pdstid,pmeantraveltime,pstandarddeviationtraveltime,pgeometricmeantraveltime,pgeometricstandarddeviationtraveltime);
		hod = pHod;
	}

	public int darHod(){
		return hod;
	}
	public String toString()
	{
		return "Sourceid: "+darSourceid()+" Destid: "+darDstid()+" Hora: "+darHod();
	}

	@Override
	public int compareTo(ViajeHora o) 
	{
		if (this.darHod()<o.darHod())
		{
			return -1;
		}
		if (this.darHod()>o.darHod())
		{
			return 1;
		}
		if (this.darHod()==o.darHod())
		{
			return 0;
		}
		else
		{
			return 0;
		}
	}
}//end ViajeHora