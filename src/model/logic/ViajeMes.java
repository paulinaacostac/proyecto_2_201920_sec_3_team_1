package model.logic;

public class ViajeMes extends Viaje implements Comparable<ViajeMes> {

	private int month;
	
	/**
	 * 
	 * @param psourceid
	 * @param pdstid
	 * @param pmonth
	 * @param pmeantraveltime
	 * @param pstandarddeviationtraveltime
	 * @param pgeometricmeantraveltime
	 * @param pgeometricstandarddeviationtraveltime
	 */
	public ViajeMes(int psourceid, int pdstid, int pmonth, double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime){
		super(psourceid,pdstid,pmeantraveltime,pstandarddeviationtraveltime,pgeometricmeantraveltime,pgeometricstandarddeviationtraveltime);
		month = pmonth;
	}

	public int darMonth(){
		return month;
	}

	@Override
	public int compareTo(ViajeMes o) {
		if(sourceid > o.sourceid) return 1;
		else if (sourceid < o.sourceid) return -1;
		else
		{
			if(destid > o.destid) return 1;
			else if (destid < o.destid) return -1;
			else {
				if (month>o.month) return 1;
				if (month<o.month) return -1;
				else return 0;
			}
		}

	}
}//end ViajeMes