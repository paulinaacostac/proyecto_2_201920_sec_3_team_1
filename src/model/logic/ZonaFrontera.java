package model.logic;

import model.data_structures.ArregloDinamico;
import model.data_structures.MaxHeapCP;
import model.data_structures.MinHeapCP;

public class ZonaFrontera implements Comparable<ZonaFrontera>
{
	private String sourceId;
	private String nombre;
	private double shape_leng;
	private double shape_area;
	private MaxHeapCP<Coordenada> coordenadasMaxHeap;
	public ZonaFrontera(String pSourceid,String pNombre, double shape_leng2, double shape_area2,MaxHeapCP<Coordenada> pCoord)
	{
		sourceId=pSourceid;
		nombre=pNombre;
		shape_leng=shape_leng2;
		shape_area=shape_area2;
		coordenadasMaxHeap=pCoord;
	}
	public String darSourceId()
	{
		return sourceId;
	}
	public String darNombre()
	{
		return nombre;
	}
	public double darPerimetro()
	{
		return shape_leng;
	}
	public double darArea()
	{
		return shape_area;
	}
	public int darNumCoordenadas()
	{
		return darCoordenadasMaxHeap().size();
	}
	public MaxHeapCP<Coordenada> darCoordenadasMaxHeap()
	{
		return coordenadasMaxHeap;
	}
	public double darLatitudMaxima()
	{
		return darCoordenadasMaxHeap().darMax().darLatitud();
	}
	
	//Comparar zonas por latitud

	public int compareTo(ZonaFrontera o) 
	{
		if (this.darNumCoordenadas()>o.darNumCoordenadas())
		{
			return 1;
		}
		if (this.darNumCoordenadas()<o.darNumCoordenadas())
		{
			return -1;
		}
		if (this.darNumCoordenadas()==o.darNumCoordenadas())
		{
			return 0;
		}
		else
		{
			return 0;
		}
	
	}

}