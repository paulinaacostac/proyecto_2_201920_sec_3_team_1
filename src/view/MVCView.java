package view;

import java.util.Iterator;

import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.data_structures.LinearProbingHashST;
import model.logic.Coordenada;
import model.logic.MVCModelo;
import model.logic.Viaje;
import model.logic.ViajeHora;
import model.logic.ViajeMes;
import model.logic.Zona;
import model.logic.ZonaFrontera;

public class MVCView 
{


		private MVCModelo modelo;
		
		private int N;
		/**
		 * Metodo constructor
		 */
		public MVCView()
		{
			modelo = new MVCModelo();
			N = 20;
		}

		public void printMenu()
		{
			System.out.println("1. Cargar");
			System.out.println("2. Req 1A");
			System.out.println("3. Req 2A");
			System.out.println("4. Req 3A");
			System.out.println("5. Req 1B");
			System.out.println("6. Req 2B");
			System.out.println("7. Req 3B");
			System.out.println("8. Req 1C");
			System.out.println("9. Req 2C");
			System.out.println("10. Req 3C");
			System.out.println("11. Req 4C");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}
		/**
		 * 
		 * @param pTotalViajesMes
		 * @param pTotalViajesDia
		 * @param pTotalViajesHora
		 * @param pZonaMenor
		 * @param pZonaMayor
		 */
		public void printCargar(int pTotalViajes, int pTotalZonas, int pTotalNodos)
		{
			System.out.println("N�mero viajes: " + pTotalViajes);
			System.out.println("N�mero zonas: " + pTotalZonas);
			System.out.println("N�mero nodos: " + pTotalNodos);
		}

		public void printLetrasMasFrecuentes(ArregloDinamico<Zona> pZonas, int N)
		{
			System.out.println("-------------------------------------------------------------------------------------------------------------------- \n");
			String letraActual = pZonas.darElemento(0).darNombre().substring(0,1);
			System.out.println("Letra: " +letraActual);
			for (int i = 0; i < pZonas.darTamano() ; i++) 
			{
				Zona zonaTemp = pZonas.darElemento(i);	
				String letraTemp = zonaTemp.darNombre().substring(0, 1);
				if (!letraActual.equals(letraTemp)) 
				{
					System.out.println("------------------ \n");
					System.out.println("Letra: " +letraTemp);
					letraActual = letraTemp;
				}
				System.out.println(zonaTemp.darNombre());
			}
			System.out.println("-------------------------------------------------------------------------------------------------------------------- \n");
		}

		public void printNodosDelimitan(ArregloDinamico<Coordenada> pNodos, ArregloDinamico<String> pNombresN)
		{
			System.out.println("-------------------------------------------------------------------------------------------------------------------- \n");
			if (pNodos.darTamano()==0) 
			{
				System.out.println("No hay nodos que coincidan del rango.");
				return;
			}
			System.out.println("El n�mero de nodos encontrados es: " + pNodos.darTamano() +"\n");
			System.out.println("Latitud | Longitud | Nombre");
			for (int i = 0; i < pNodos.darTamano() && i<N; i++) 
			{
				Coordenada coord = pNodos.darElemento(i); 
				System.out.println(coord.darLatitud() + "|" + coord.darLongitud() + "|" + pNombresN.darElemento(i));
				if (i==N-1) 
				{
					System.out.println("...");
				}	
			}
			System.out.println("-------------------------------------------------------------------------------------------------------------------- \n");

		}

		public void printViajesMesTiempo(ArregloDinamico<ViajeMes> pViajes)
		{
			if (pViajes.darTamano()==0) 
			{
				System.out.println("No hay viajes dentro del rango.");
				return;
			}
			System.out.println("SourceID | DestID | Month | MeanTime");
			for (int i = 0; i < pViajes.darTamano() && i < N; i++) 
			{
				ViajeMes viaje = pViajes.darElemento(i);
				System.out.println(viaje.darSourceid()+"|"+viaje.darDstid() +"|"+ viaje.darMonth() + "|" + viaje.darMeanTravelTime());
				if (i==19)
					System.out.println("...");
			}
			System.out.println("-------------------------------------------------------------------------------------------------------------------- \n");
		}

		public void printZonasNorte(ArregloDinamico<Zona> pZonas)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("N Zonas mas al norte:");
			for (int i=0;i<pZonas.darTamano();i++)
			{
				System.out.println("Zona "+(i+1)+"- "+"Nombre: "+pZonas.darElemento(i).darNombre()+" / Coordenada mas al norte: "+"[ "+pZonas.darElemento(i).darCoordenadasMaxHeap().darMax().darLatitud()+", "+pZonas.darElemento(i).darCoordenadasMaxHeap().darMax().darLongitud()+" ]");
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
		}

		public void printNodosMalla(LinearProbingHashST<Integer,Esquina> pNodos)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("Esquinas con esa longitud y latitud:");
			Iterator<Integer> iter=pNodos.keys();
			int i=1;
			while (iter.hasNext())
			{
				Integer a=iter.next();
				System.out.println("Esquina "+i+": "+ "id: "+pNodos.get(a).darpIdNodo()+" Latitud: "+pNodos.get(a).darLatitud()+" Longitud: "+pNodos.get(a).darLongitud());
				i++;
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
		}

		public void printViajesMesDesviacion(ArregloDinamico<ViajeMes> pViajes)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("Viajes en el rango de desviacion:");
			for (int i=0;i<pViajes.darTamano();i++)
			{
				System.out.println("Viaje "+(i+1)+"- "+"Sourceid: "+pViajes.darElemento(i).darSourceid()+" / Destid: "+pViajes.darElemento(i).darDstid()+" / Mes: "+pViajes.darElemento(i).darMonth()+" / Desviacion estandar: "+pViajes.darElemento(i).darStandardDeviationTravelTime());
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");

		}

		public void printViajesHoraSource(ArregloDinamico<ViajeHora> pViajes)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("Viajes con sourceid y hora dada:");
			for (int i=0;i<pViajes.darTamano();i++)
			{
				ViajeHora vh=pViajes.darElemento(i);
				System.out.println("Zona origen: "+vh.darSourceid()+" Zona destino: "+vh.darDstid()+" Hora: "+vh.darHod()+" Tiempo promedio: "+vh.darMeanTravelTime());
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");

		}

		public void printViajesHoraDest(ArregloDinamico<ViajeHora> pViajes)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("Viajes con destid y hora dada:");
			for (int i=0;i<pViajes.darTamano();i++)
			{
				ViajeHora vh=pViajes.darElemento(i);
				System.out.println("Viaje "+(i+1)+": "+"Zona origen: "+vh.darSourceid()+" Zona destino: "+vh.darDstid()+" Hora: "+vh.darDow()+"Tiempo promedio: "+vh.darMeanTravelTime());
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");


		}

		public void printZonasPriorizadas(ArregloDinamico<ZonaFrontera> pZonas)
		{
			System.out.println("--------------------------------------------------------------------------------------------------------------------");
			System.out.println("Viajes con destid y hora dada:");
			for (int i=0;i<pZonas.darTamano();i++)
			{
				ZonaFrontera vh=pZonas.darElemento(i);
				System.out.println("Zona "+(i+1)+": "+"Nombre: "+vh.darNombre()+" Numero Coordenadas(fronteras) : "+vh.darNumCoordenadas());
			}
			System.out.println("--------------------------------------------------------------------------------------------------------------------");

		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		

	}
