package test.data_structures;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ArregloDinamico;
import model.data_structures.MaxHeapCP;
import model.logic.MVCModelo;
import model.logic.Viaje;

public class TestMaxHeapCP 
{
	private MaxHeapCP<Integer> heapCP;
	private ArregloDinamico<Integer> aux;
	private static int CAPACIDAD=10;
	
	@Before
	public void setUp1() 
	{
		aux = new ArregloDinamico<Integer>(CAPACIDAD);
		heapCP = new MaxHeapCP<Integer>(CAPACIDAD);
	}
	public void setUp2() 
	{
		for(int i =0; i< CAPACIDAD;i++)
		{
			aux.agregar(i);
		}
		//randomizar el arreglo
		for(int i=0;i<CAPACIDAD;i++)
		{
			int aleatorio=0;
			aleatorio=(int)(Math.random()*(CAPACIDAD-1));
			aux.exch(i, aleatorio);
		}
	}
	@Test
	public void testInsertarSacar() 
	{
		setUp1();
		setUp2();
		for (int i=0;i<aux.darTamano();i++)
		{
			heapCP.insertar(aux.darElemento(i));
		}
		assertTrue(heapCP.sacarMax()==(Integer)9);
		assertTrue(heapCP.sacarMax()==(Integer)8);
		assertTrue(heapCP.sacarMax()==(Integer)7);
	}

}