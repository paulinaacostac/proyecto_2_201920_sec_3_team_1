package test.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.RedBlackBST;;

public class TestRedBlackBST 
{

	private RedBlackBST<String, Integer> tree;

	@Before
	public void setUp1() 
	{
		tree = new RedBlackBST<String, Integer>();
	}

	//En orden
	public void setUp2() 
	{
		tree.put("a", 1);
		tree.put("b", 2);
		tree.put("c", 3);
		tree.put("d", 4);
		tree.put("e", 5);
		tree.put("f", 6);
	}

	//Desorden
	public void setUp3() 
	{
		tree.put("f", 6);
		tree.put("b", 2);
		tree.put("d", 4);
		tree.put("c", 3);
		tree.put("e", 5);
		tree.put("a", 1);
	}

	@Test
	public void testGet2() {
		setUp2();
		assertTrue(tree!=null);
		assertEquals(tree.size(),6);
		assertEquals((Integer)1,tree.get("a"));
		assertEquals((Integer)2,tree.get("b"));
		assertEquals((Integer)3,tree.get("c"));
		assertEquals((Integer)4,tree.get("d"));
		assertEquals((Integer)5,tree.get("e"));
		assertEquals((Integer)6,tree.get("f"));
	}
	
	@Test
	public void testGet3() {
		setUp3();
		assertTrue(tree!=null);
		assertEquals(tree.size(),6);
		assertEquals((Integer)1,tree.get("a"));
		assertEquals((Integer)2,tree.get("b"));
		assertEquals((Integer)3,tree.get("c"));
		assertEquals((Integer)4,tree.get("d"));
		assertEquals((Integer)5,tree.get("e"));
		assertEquals((Integer)6,tree.get("f"));
	}

	@Test
	public void testRedBlackBST2()
	{
		setUp2();
		assertTrue(tree!=null);
		assertTrue(tree.check());
		assertEquals(Math.floor(Math.log(6)/Math.log(2)), tree.heightBlack(), 0.1);
	}
	
	@Test
	public void testRedBlackBST3()
	{
		setUp3();
		assertTrue(tree!=null);
		assertTrue(tree.check());
		assertEquals(Math.floor(Math.log(6)/Math.log(2)), tree.heightBlack(), 0.1);
	}
	
	



}
